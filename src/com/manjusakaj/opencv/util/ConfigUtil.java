package com.manjusakaj.opencv.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 说明：读取配置
 *
 * @author 林耀均
 * @date 2022年01月05日 16:36
 */
public class ConfigUtil {
    private final static Properties properties = new Properties();

    static {
        //通过类装载器得到流数据
        InputStream inputStream = ConfigUtil.class.getClassLoader().getResourceAsStream("conf.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取OpenCV dll文件路径
     * @return
     */
    public static String getOpenCvDllDir() {
        return properties.getProperty("opencv.dll.dir", "");
    }
}
