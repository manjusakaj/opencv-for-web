package com.manjusakaj.opencv.listener;

import com.manjusakaj.opencv.util.ConfigUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 说明：OpenCV监听器（启动时自动加载相关dll文件）
 *
 * @author 林耀均
 * @date 2022年01月05日 16:32
 */
public class InitOpencvListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

    public void contextInitialized(ServletContextEvent arg0) {
        System.out.println("=====================开始加载OpenCV dll文件===============================");
        System.load(ConfigUtil.getOpenCvDllDir());
        System.out.println("=====================加载OpenCV dll文件完成===============================");
    }
}
