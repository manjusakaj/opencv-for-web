package com.manjusakaj.opencv.handler;

import com.manjusakaj.opencv.util.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 说明：图像处理器（基于OpenCV）
 *
 * @author 林耀均
 * @date 2022年01月05日 16:28
 */
public class ImageHandler {
    // 正在展示的图片
    public static Mat showMat;
    // 等待处理的图片
    public static Mat dealMat;
    // 主栏图片
    public static List<Mat> mainMats = new ArrayList<>();
    public static int mainMatsIndex = -1;
    // 副栏图片
    public static Map<Integer, List<Mat>> subMatMaps = new HashMap<>();
    public static int subMatsIndex = -1;

    /**
     * 局部放大
     * @return
     */
    public static Map<String, Object> partBigger(double xDouble, double yDouble) throws IOException {
        Map<String, Object> result = new HashMap<>();
        int x = (int) xDouble;
        int y = (int) yDouble;

        Mat img_temp = showMat.clone();
        int foo = 10;//定义尺寸
        int fooResize = 2 * foo;//最终显示放大镜的正方形区域的边长，用作下面if的判断

        int width = fooResize;
        int height = fooResize;
        int strat_x = x - foo;
        int strat_y = y - foo;
        int markBorderTop = 0;
        int markBorderBottom = 0;
        int markBorderLeft = 0;
        int markBorderRight = 0;
        if(x + foo > img_temp.cols()) {
            width = foo + img_temp.cols() - x;
            markBorderRight = fooResize - width;
        }
        if(y + foo > img_temp.rows()) {
            height = foo + img_temp.rows() - y;
            markBorderBottom = fooResize - height;
        }
        if(strat_x < 0) {
            width = width + strat_x;
            markBorderLeft = Math.abs(strat_x);
            strat_x = 0;
        }
        if(strat_y < 0) {
            height = height + strat_y;
            markBorderTop = Math.abs(strat_y);
            strat_y = 0;
        }
        Mat imgROI = new Mat(img_temp, new Rect(strat_x, strat_y, width, height));
        Mat roiResize = new Mat();
        Imgproc.resize(imgROI, roiResize, new Size(10 * imgROI.cols(), 10 * imgROI.rows()), 0, 0, Imgproc.INTER_NEAREST);
        Core.copyMakeBorder(roiResize, roiResize, 10 * markBorderTop, 10 * markBorderBottom, 10 * markBorderLeft, 10 * markBorderRight, Core.BORDER_CONSTANT, new Scalar(255, 255, 255));  //标注鼠标的位置

        int size = 50;
        //绘制横线
        Imgproc.line(roiResize, new Point((roiResize.cols() - size) / 2, roiResize.rows() /2), new Point((roiResize.cols() + size) / 2, roiResize.rows() / 2), new Scalar(0,0,0));
        //绘制竖线
        Imgproc.line(roiResize, new Point(roiResize.cols() / 2, (roiResize.rows() - size) / 2), new Point(roiResize.cols() / 2, (roiResize.rows() + size) / 2), new Scalar(0,0,0));
        result.put("pic", Utils.mat2Base64(roiResize));

        double[] colors = showMat.get(y, x);
        if(colors != null && colors.length == 3) {
            result.put("R", new Double(colors[2]).intValue());
            result.put("G", new Double(colors[1]).intValue());
            result.put("B", new Double(colors[0]).intValue());
            result.put("color", Utils.convertRGBToHex(new Double(colors[2]).intValue(), new Double(colors[1]).intValue(), new Double(colors[0]).intValue()));
        }
        return result;
    }

    /**
     * 旋转
     * @param src
     * @param angle
     * @param scale
     * @return
     */
    public static Map<String, Object> rotate(Mat src, double angle, double scale) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = src.clone();
        int width = src.width();
        int height = src.height();
        //复制矩阵进入dst
        Point center = new Point(width / 2.0, height / 2.0);
        Mat affineTrans = Imgproc.getRotationMatrix2D(center, angle, scale);

        double radians = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(radians));
        double cos = Math.abs(Math.cos(radians));
        double newHeight = sin * width + cos * height;
        double newWidth = cos * width + sin * height;
        // 改变变换矩阵第三列的值
        affineTrans.put(0, 2, affineTrans.get(0, 2)[0] + (newWidth - width) / 2);
        affineTrans.put(1, 2, affineTrans.get(1, 2)[0] + (newHeight - height) / 2);

        Imgproc.warpAffine(src, dst, affineTrans, new Size(newWidth, newHeight));
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;

    }

    /**
     * 二值化
     * @param thresh
     * @param maxval
     * @param thresholdType
     * @return
     * @throws IOException
     */
    public static Map<String, Object> threshold(double thresh, double maxval, int thresholdType) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = new Mat();
        // 先转灰度图
        Imgproc.cvtColor(ImageHandler.dealMat, dst, Imgproc.COLOR_BGR2GRAY,0);
        Imgproc.threshold(dst, dst, thresh, maxval, thresholdType);
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;
    }

    /**
     * 灰度图
     * @return
     */
    public static Map<String, Object> toGray() throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = new Mat();
        Imgproc.cvtColor(ImageHandler.dealMat, dst, Imgproc.COLOR_BGR2GRAY,0);
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;
    }

    /**
     * 灰度图
     * @return
     */
    public static Map<String, Object> toHsv(String toHsvType) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = new Mat();
        Imgproc.cvtColor(ImageHandler.dealMat, dst, Imgproc.COLOR_RGB2HSV,0);
        List<Mat> mv = new ArrayList<>();// 分离出来的彩色通道数据
        Core.split(dst, mv);// 分离色彩通道
        if(!"-1".equals(toHsvType)) {
            dst = mv.get(Integer.parseInt(toHsvType));
        }
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;
    }

    /**
     * 灰度图
     * @return
     */
    public static Map<String, Object> toHls(String toHlsType) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = new Mat();
        Imgproc.cvtColor(ImageHandler.dealMat, dst, Imgproc.COLOR_BGR2HLS,0);
        List<Mat> mv = new ArrayList<>();// 分离出来的彩色通道数据
        Core.split(dst, mv);// 分离色彩通道
        if(!"-1".equals(toHlsType)) {
            dst = mv.get(Integer.parseInt(toHlsType));
        }
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;
    }

    /**
     * 灰度图
     * @return
     */
    public static Map<String, Object> toRgb(String toRgbType) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = new Mat();
        Imgproc.cvtColor(ImageHandler.dealMat, dst, Imgproc.COLOR_BGR2RGB,0);
        List<Mat> mv = new ArrayList<>();// 分离出来的彩色通道数据
        Core.split(dst, mv);// 分离色彩通道
        if(!"-1".equals(toRgbType)) {
            dst = mv.get(Integer.parseInt(toRgbType));
        }
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;
    }

    /**
     * 直方图
     * @return
     */
    public static Map<String, Object> calcHist() {
        Map<String, Object> result = new LinkedHashMap<>();
        int channels = ImageHandler.dealMat.channels();
        for (int i = 0; i < channels; i++) {
            List<Integer> list = new ArrayList<>();

            Mat hist = new Mat();
            Imgproc.calcHist(Collections.singletonList(ImageHandler.dealMat), new MatOfInt(i), new Mat(), hist, new MatOfInt(256), new MatOfFloat(0, 256));
            for (int j = 0; j < hist.rows(); j++) {
                list.add((int) hist.get(j, 0)[0]);
            }
            result.put("第" + (i + 1) + "信道", list);
        }
        return result;
    }

    /**
     * 轮廓检测
     * @return
     */
    public static Map<String, Object> canny(double threshold1, double threshold2, int apertureSize, boolean L2gradient) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Mat dst = new Mat();
        Imgproc.cvtColor(ImageHandler.dealMat, dst, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Canny(dst, dst, threshold1, threshold2, apertureSize, L2gradient);
        result.put("pic", Utils.mat2Base64(dst));
        ImageHandler.showMat = dst;
        return result;
    }

    /**
     * 记录mats
     */
    public static Map<String, Object> recordMats(String type, int mainIndex, int subIndex, Mat uploadMat) throws IOException {
        List<Mat> subMats;
        switch (type) {
            case "upload":
                ImageHandler.mainMats.add(uploadMat.clone());
                subMats = ImageHandler.subMatMaps.getOrDefault(ImageHandler.mainMats.size(), new ArrayList<>());
                subMats.add(uploadMat.clone());
                ImageHandler.subMatMaps.put(ImageHandler.mainMats.size() - 1, subMats);
                ImageHandler.showMat = uploadMat.clone();
                ImageHandler.dealMat = ImageHandler.showMat.clone();
                ImageHandler.mainMatsIndex = ImageHandler.mainMats.size() - 1;
                ImageHandler.subMatsIndex = 0;
                break;
            case "addMain":
                ImageHandler.mainMats.add(ImageHandler.showMat.clone());
                subMats = ImageHandler.subMatMaps.getOrDefault(ImageHandler.mainMats.size(), new ArrayList<>());
                subMats.add(ImageHandler.showMat.clone());
                ImageHandler.subMatMaps.put(ImageHandler.mainMats.size() - 1, subMats);
                break;
            case "addSub":
                subMats = ImageHandler.subMatMaps.getOrDefault(mainIndex, new ArrayList<>());
                subMats.add(ImageHandler.showMat.clone());
                ImageHandler.subMatMaps.put(mainIndex, subMats);
                break;
            case "click":
                ImageHandler.mainMatsIndex = mainIndex;
                ImageHandler.subMatsIndex = subIndex;
                ImageHandler.showMat = ImageHandler.subMatMaps.get(mainIndex).get(subIndex).clone();
                ImageHandler.dealMat = ImageHandler.subMatMaps.get(mainIndex).get(subIndex).clone();
                break;
            case "clearMain":
                ImageHandler.mainMatsIndex = -1;
                ImageHandler.subMatsIndex = -1;
                ImageHandler.mainMats = new ArrayList<>();
                ImageHandler.subMatMaps = new HashMap<>();
                ImageHandler.showMat = null;
                ImageHandler.dealMat = null;
                break;
            case "clearSub":
                ImageHandler.subMatsIndex = 0;
                Mat subMat = ImageHandler.subMatMaps.get(ImageHandler.mainMatsIndex).get(0).clone();
                ImageHandler.subMatMaps.get(ImageHandler.mainMatsIndex).clear();
                ImageHandler.subMatMaps.get(ImageHandler.mainMatsIndex).add(subMat);
                ImageHandler.showMat = ImageHandler.subMatMaps.get(ImageHandler.mainMatsIndex).get(0).clone();
                ImageHandler.dealMat = ImageHandler.subMatMaps.get(ImageHandler.mainMatsIndex).get(0).clone();
                break;
        }
        Map<String, Object> result = new HashMap<>();
        if(ImageHandler.showMat != null) {
            result.put("showMat", Utils.mat2Base64(ImageHandler.showMat));
        }
        if(ImageHandler.dealMat != null) {
            result.put("dealMat", Utils.mat2Base64(ImageHandler.dealMat));
        }
        result.put("mainMats", Utils.mat2Base64More(ImageHandler.mainMats));
        result.put("subMatMaps", Utils.mat2Base64MoreMap(ImageHandler.subMatMaps));
        result.put("mainMatsIndex", ImageHandler.mainMatsIndex);
        result.put("subMatsIndex", ImageHandler.subMatsIndex);
        return result;
    }

    /**
     * 初始化mats
     */
    public static void initRecordMats() throws IOException {
        ImageHandler.showMat = null;
        ImageHandler.dealMat = null;
        ImageHandler.mainMats = new ArrayList<>();
        ImageHandler.mainMatsIndex = -1;
        ImageHandler.subMatMaps = new HashMap<>();
        ImageHandler.subMatsIndex = -1;
    }
}