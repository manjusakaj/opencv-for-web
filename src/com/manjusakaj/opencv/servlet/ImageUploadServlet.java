package com.manjusakaj.opencv.servlet;

import com.manjusakaj.opencv.handler.ImageHandler;
import com.manjusakaj.opencv.util.Utils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

/**
 * 说明：请求处理器
 *
 * @author 林耀均
 * @date 2022年01月05日 16:55
 */
public class ImageUploadServlet extends HttpServlet {

    /**
     * 上传数据及保存文件
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置response的编码及格式
        response.setContentType("text/json; charset=utf-8");
        PrintWriter out = response.getWriter();

        String realPath = this.getServletContext().getRealPath("/temp");

        //判断存放上传文件的目录是否存在（不存在则创建）
        File f = new File(realPath);
        if (!f.exists() && !f.isDirectory()) {
            f.mkdir();
        }

        // 创建DiskFileItemFactory对象
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // 创建ServletFileUpload对象
        ServletFileUpload servletFileUpload = new ServletFileUpload(factory);

        try {
            List<FileItem> fileItems = servletFileUpload.parseRequest(request);
            for (FileItem fileItem : fileItems) {
                //                处理上传的文件
                String uploadFileName = fileItem.getName();
                if (uploadFileName.trim().equals("")) {
                    continue;
                }
                String fileName = uploadFileName.substring(uploadFileName.lastIndexOf("/") + 1);
                String[] fileNameArr = fileName.split("\\.");
                UUID uuid = UUID.randomUUID();

                fileName = uuid.toString();
                if(fileNameArr.length == 2) {
                    fileName = fileName + "." + fileNameArr[1];
                }
                InputStream inputStream = fileItem.getInputStream();

                FileOutputStream outputStream = new FileOutputStream(realPath + "/" + fileName);
                byte[] buffer = new byte[1024 * 1024];
                int length;
                while ((length = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }
                outputStream.close();
                outputStream.close();

                Mat uploadMat = Imgcodecs.imread(realPath + "/" + fileName);
                ImageHandler.recordMats("upload",-1, -1, uploadMat);

                out.print(new ResultData(true, Utils.mat2Base64(uploadMat)));
            }
        } catch (FileUploadException e) {
            e.printStackTrace();
            out.print(new ResultData(false, e.getMessage()));
        }
    }
}
