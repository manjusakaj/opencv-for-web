package com.manjusakaj.opencv.servlet;

import com.google.gson.Gson;
import com.manjusakaj.opencv.handler.ImageHandler;
import org.apache.commons.lang.StringUtils;
import org.opencv.core.Mat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;

/**
 * 说明：请求处理器
 *
 * @author 林耀均
 * @date 2022年01月05日 16:55
 */
public class ImageHandlerServlet extends HttpServlet {

    @SuppressWarnings({"rawtypes"})
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置request字符编码
        req.setCharacterEncoding("utf-8");
        // 设置response的编码及格式
        resp.setContentType("text/json; charset=utf-8");
        PrintWriter out = resp.getWriter();

        // 方法名称
        String methodName = req.getParameter("m");
        String params = req.getParameter("params");

        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if(StringUtils.isNotBlank(params)) {
            linkedHashMap = new Gson().fromJson(params, LinkedHashMap.class);
        }
        switch (methodName) {
            case "initRecordMats":
                ImageHandler.initRecordMats();
                break;
            case "recordMats":
                out.print(new Gson().toJson(ImageHandler.recordMats(linkedHashMap.get("type").toString(), (int) Double.parseDouble(linkedHashMap.get("mainIndex").toString()), (int) Double.parseDouble(linkedHashMap.get("subIndex").toString()), null)));
                break;
            case "partBigger":
                out.print(new Gson().toJson(ImageHandler.partBigger(Double.parseDouble(linkedHashMap.get("x").toString()), Double.parseDouble(linkedHashMap.get("y").toString()))));
                break;
            case "rotate":
                Mat src = "show".equals(linkedHashMap.get("matType").toString()) ? ImageHandler.showMat : ImageHandler.dealMat;
                out.print(new Gson().toJson(ImageHandler.rotate(src, Double.parseDouble(linkedHashMap.get("angle").toString()), Double.parseDouble(linkedHashMap.get("scale").toString()))));
                break;
            case "threshold":
                out.print(new Gson().toJson(ImageHandler.threshold(Double.parseDouble(linkedHashMap.get("thresh").toString()), Double.parseDouble(linkedHashMap.get("maxval").toString()), Integer.parseInt(linkedHashMap.get("thresholdType").toString()))));
                break;
            case "rotateCustom":
                out.print(new Gson().toJson(ImageHandler.rotate(ImageHandler.dealMat, Double.parseDouble(linkedHashMap.get("angle").toString()), 1)));
                break;
            case "toGray":
                out.print(new Gson().toJson(ImageHandler.toGray()));
                break;
            case "toHsv":
                out.print(new Gson().toJson(ImageHandler.toHsv(linkedHashMap.get("toHsvType").toString())));
                break;
            case "toHls":
                out.print(new Gson().toJson(ImageHandler.toHls(linkedHashMap.get("toHlsType").toString())));
                break;
            case "toRgb":
                out.print(new Gson().toJson(ImageHandler.toRgb(linkedHashMap.get("toRgbType").toString())));
                break;
            case "calcHist":
                out.print(new Gson().toJson(ImageHandler.calcHist()));
                break;
            case "canny":
                out.print(new Gson().toJson(ImageHandler.canny(Double.parseDouble(linkedHashMap.get("canny-threshold1").toString()), Double.parseDouble(linkedHashMap.get("canny-threshold2").toString()), Integer.parseInt(linkedHashMap.get("canny-apertureSize").toString()), Boolean.parseBoolean(linkedHashMap.get("canny-L2gradient").toString()))));
                break;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
}
