package com.manjusakaj.opencv.servlet;

import com.google.gson.Gson;

/**
 * 说明：
 *
 * @author 林耀均
 * @date 2022年01月18日 16:30
 */
public class ResultData {
    private boolean result;
    private Object data;

    public ResultData(boolean result, Object data) {
        this.result = result;
        this.data = data;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
