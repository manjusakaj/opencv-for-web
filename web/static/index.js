var partBiggerFlag = true, x, y, pageX, pageY;

$(function() {
    init();
    $('select').selectpicker();
    $('.form-range').change(function() {
        $(this).parent().find('input[type=number]').val($(this).val());
    });
    $('input[type=number]').change(function() {
        if(Number($(this).val()) > Number($(this).attr('max'))) {
            $(this).val($(this).attr('max'))
        }
        if(Number($(this).val()) < Number($(this).attr('min'))) {
            $(this).val($(this).attr('min'))
        }
        var _target = $(this).parent().find('.form-range');
        if(_target) {
            _target.val($(this).val());
        }
    });
    $('.opencvDiv li').click(function() {
        $('.opencvDiv li').removeClass('active');
        $(this).addClass('active');
    });

    $('#fileUpload').change(function() {
        var file = this.files[0];
        if(!file) {
            return;
        }
        //这里我们判断下类型如果不是图片就返回 去掉就可以上传任意文件
        if(!/image\/\w+/.test(file.type)){
            alert("请上传图片");
            return false;
        }
        $.ajax({
            url: "/upload",
            type: 'POST',
            cache: false,
            data: new FormData($('#fileUpload').closest('form')[0]),
            processData: false,
            contentType: false,
            dataType:"json",
            success: function(res) {
                if(res.result) {
                    showMainSubPics('click', $('.mainPicDiv img').length, 0);
                    $('#fileUpload').val('');
                }
            }
        });
    });

    $('.picPrevDiv img').mousemove(function(e) {
        x = e.offsetX;
        y = e.offsetY;
        pageX = e.pageX;
        pageY = e.pageY;
        partBigger(x, y, pageX, pageY);
    });
    $('.picPrevDiv img').mouseleave(function(e) {
        setTimeout(function() {
            $('.partBiggerDiv').hide();
        }, 50);
    });

    $(document).keydown(function(event){
        if(event.keyCode === 37) {   // left
            if(!$('.partBiggerDiv').is(':hidden')) {
                x = x - 1;
                pageX = pageX - 1;
                if(x < 0) {
                    $('.partBiggerDiv').hide();
                    return;
                }
                partBigger(x, y, pageX, pageY);
            }
        } else if (event.keyCode === 38){    // top
            if (!$('.partBiggerDiv').is(':hidden')) {
                y = y - 1;
                pageY = pageY - 1;
                if(y < 0) {
                    $('.partBiggerDiv').hide();
                    return;
                }
                partBigger(x, y, pageX, pageY);
            }
        } else if (event.keyCode === 39){    // right
            if(!$('.partBiggerDiv').is(':hidden')) {
                x = x + 1;
                pageX = pageX + 1;
                if(x > $('.picPrevDiv img').width()) {
                    $('.partBiggerDiv').hide();
                    return;
                }
                partBigger(x, y, pageX, pageY);
            }
        } else if (event.keyCode === 40){    // down
            if(!$('.partBiggerDiv').is(':hidden')) {
                y = y + 1;
                pageY = pageY + 1;
                if(y > $('.picPrevDiv img').height()) {
                    $('.partBiggerDiv').hide();
                    return;
                }
                partBigger(x, y, pageX, pageY);
            }
        }
    });
});

function partBigger(x, y, pageX, pageY) {
    if(!partBiggerFlag) {
        return;
    }
    partBiggerFlag = false;
    var params = {
        x: x,
        y: y,
    };

    var offset = 50;

    $.post('/handler', {
        m: 'partBigger',
        params: JSON.stringify(params),
    }, function(_res) {
        var left = pageX + offset;
        var top = pageY + offset;
        if(top + 300 > document.documentElement.clientHeight) {
            top = pageY - offset - 300
        }
        if(left + 200 > document.documentElement.clientWidth) {
            left = pageX - offset - 200
        }
        $('.partBiggerDiv').css({left: left, top: top});
        $('.partBiggerDiv img').attr('src', _res.pic);
        $('.partBiggerDiv .pointXSpan').html(x);
        $('.partBiggerDiv .pointYSpan').html(y);
        $('.partBiggerDiv .colorSpan').html(_res.color);
        $('.partBiggerDiv .colorShowSpan').css('background-color', _res.color);
        $('.partBiggerDiv .rgbSpan').html(_res.R + "," + _res.G + "," + _res.B);
        $('.partBiggerDiv').show();
        partBiggerFlag = true;
    }, 'json').fail(function(response) {
        partBiggerFlag = true;
    });
}

function selectPic() {
    $('#fileUpload').click();
}

function showBigImg(base64Str) {
    $('.picPrevDiv img').attr('src', base64Str);
}

function showMainSubPics(type, mainIndex, subIndex) {
    if(type == 'addSub' || type == 'clearSub') {
        mainIndex = $('.mainPicDiv img.active').index();
    }
    var params = {
        type: type,
        mainIndex: mainIndex,
        subIndex: subIndex,
    };

    $.post('/handler', {
        m: 'recordMats',
        params: JSON.stringify(params),
    }, function(_res) {
        showBigImg(_res.showMat);
        $('.mainPicDiv').empty();
        $('.subPicDiv').empty();
        for (let i = 0; i < _res.mainMats.length; i++) {
            if(i == _res.mainMatsIndex) {
                $('.mainPicDiv').append('<img class="active" src="'+_res.mainMats[i]+'" onclick="showMainSubPics(\'click\', '+i+', 0)"/>');
                for (let j = 0; j < _res.subMatMaps[i].length; j++) {
                    if(j == _res.subMatsIndex) {
                        $('.subPicDiv').append('<img class="active" src="'+_res.subMatMaps[i][j]+'" onclick="showMainSubPics(\'click\', '+i+', '+j+')"/>');
                    } else {
                        $('.subPicDiv').append('<img src="'+_res.subMatMaps[i][j]+'" onclick="showMainSubPics(\'click\', '+i+', '+j+')"/>');
                    }
                }
            } else {
                $('.mainPicDiv').append('<img src="'+_res.mainMats[i]+'" onclick="showMainSubPics(\'click\', '+i+', 0)"/>');
            }
        }
    }, 'json');
}

function downloadFile(fileName) { //下载base64图片
    var content = $('.picPrevDiv img').attr('src');
    if(!content) {
        return;
    }
    var imgs = $('.picPrevDiv img');
    var a = document.createElement('a');
    a.download = new Date().getTime() + '.jpg' || '';//这边是文件名，可以自定义
    a.href = imgs[0].src;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function rotate(matType, angle, scale) {
    var content = $('.picPrevDiv img').attr('src');
    if(!content) {
        return;
    }
    var params = {
        matType: matType,
        angle: angle,
        scale: scale,
    };
    $.post('/handler', {
        m: 'rotate',
        params: JSON.stringify(params),
    }, function(_res) {
        $('.picPrevDiv img').attr('src', _res.pic);
    }, 'json');
}

function showOpArea(method) {
    $('.opForm').show();
    $('.form-group').hide();
    $('.opButtons').show();
    $('.m-' + method).show();
}

function runOp() {
    var params = {};
    var method = $('.opencvDiv li.active').data('method');
    var paramArr = $('.opForm').serializeArray();
    for(var i = 0; i < paramArr.length; i++) {
        params[paramArr[i].name] = paramArr[i].value;
    }

    $.post('/handler', {
        m: method,
        params: JSON.stringify(params),
    }, function(_res) {
        if(method === 'calcHist') {
            $('#calcHistModal').modal();
            drawCalcHist(_res);
            return;
        }
        $('.picPrevDiv img').attr('src', _res.pic);
    }, 'json');
}

function drawCalcHist(_res) {
    var legendDatas = [];
    var xDatas = [];
    var series = [];
    for(var i = 0; i < 256; i++) {
        xDatas.push(i);
    }
    for(var key in _res) {
        legendDatas.push(key);
        series.push({
            name: key,
            type: 'line',
            data: _res[key]
        });
    }

    var myChart = echarts.init(document.getElementById('calcHistChart'));

    var option = {
        title: {
            text: '颜色分布直方图'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: legendDatas
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: xDatas
        },
        yAxis: {
            type: 'value'
        },
        series: series
    };
    myChart.setOption(option);
    window.addEventListener("resize", function () {
        setTimeout(function(){
            myChart.resize();
        },300)
    });
}

function init() {
    $.post('/handler', {m: 'initRecordMats'});
}